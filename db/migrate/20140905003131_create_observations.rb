class CreateObservations < ActiveRecord::Migration
  def change
    create_table :observations do |t|
      t.integer :theme_id
      t.text :body

      t.timestamps
    end
  end
end
