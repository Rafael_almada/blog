class CreateBodies < ActiveRecord::Migration
  def change
    create_table :bodies do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
