json.array!(@observations) do |observation|
  json.extract! observation, :id, :theme_id, :body
  json.url observation_url(observation, format: :json)
end
