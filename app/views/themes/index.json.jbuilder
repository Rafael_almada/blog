json.array!(@themes) do |theme|
  json.extract! theme, :id, :title, :body
  json.url theme_url(theme, format: :json)
end
