json.array!(@bodies) do |body|
  json.extract! body, :id, :post_id, :body
  json.url body_url(body, format: :json)
end
